package com.cpn.flickr.utils;

import android.icu.util.DateInterval;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateManager {

    public static String getCurrentDateTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
        String currentDate = null;
        currentDate = dateFormat.format(new Date());
        return currentDate;
    }

    public static String getFormattedDateForServerFromTimeStamp(long timeStampDate){
        SimpleDateFormat dateFormatServer = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
        Date date = new Date(timeStampDate);
        String sDateFormatServer = dateFormatServer.format(date);
        return sDateFormatServer;
    }
}

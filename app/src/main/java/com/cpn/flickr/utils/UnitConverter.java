package com.cpn.flickr.utils;

import android.content.res.Resources;

/**
 * Created by Carlos on 22/02/2017.
 */

public class UnitConverter {
    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToSp(int px){
        return (int) (px / Resources.getSystem().getDisplayMetrics().scaledDensity);
    }

    public static int spToPx(int sp){
        return (int) (sp * Resources.getSystem().getDisplayMetrics().scaledDensity);
    }
}
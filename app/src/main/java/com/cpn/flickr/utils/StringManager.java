package com.cpn.flickr.utils;

import android.content.Context;

public class StringManager {
	private Context context;
	private static StringManager singleton;
	
	public static StringManager create(Context context){
		if(singleton == null){
			singleton = new StringManager(context);
		}
		return singleton;
	}
	
	private StringManager(Context context){
		this.context = context;
	}
	
	public String getString(int id){
		return context.getString(id);
	}

	public String[] getStringArray(int id){
		return context.getResources().getStringArray(id);
	}
	
	public static StringManager getInstance(){
		return singleton;
	}
	
	public void destroy(){
		singleton = null;
	}
}
package com.cpn.flickr;

public enum ActionWindow {
	SHOW_LIST_IMAGES, SHOW_DETAIL_IMAGE, SHOW_ZOOM_IMAGE;
}

package com.cpn.flickr.model;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Image implements Serializable {
    private String title;
    private String author;
    private String url;
    private String date;
    private String description;
    private Bitmap imageBitmap;

    public Image(String title, String author, String url, String date, String description) {
        this.title = title;
        this.author = author;
        this.url = url;
        this.date = date;
        this.description = description;
        this.imageBitmap = null;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getUrl() {
        return this.url;
    }

    public Bitmap getImageBitmap() { return this.imageBitmap; }

    public void setImageBitmap(Bitmap imageBitmap) { this.imageBitmap = imageBitmap; }

    public String getDate() { return this.date; }

    public String getDescription() { return description; }
}

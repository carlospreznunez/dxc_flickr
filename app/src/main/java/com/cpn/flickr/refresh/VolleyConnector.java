package com.cpn.flickr.refresh;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cpn.flickr.progressbar.ProgressBar;

/**
 * Created by Carlos on 11/01/2017.
 */

public class VolleyConnector {
    private static VolleyConnector singleton = null;
    private AppCompatActivity activity;
    private RequestQueue mRequestQueue;
    private ProgressBar progressBar;
    private Boolean showProgressBar;

    private VolleyConnector(AppCompatActivity activity, Boolean showProgressBar) {
        mRequestQueue = Volley.newRequestQueue(activity.getApplicationContext());
        this.activity = activity;
        this.showProgressBar = showProgressBar;
        progressBar = new ProgressBar(activity);
    }

    public static VolleyConnector getInstance(AppCompatActivity activity, Boolean showProgressBar) {
        if (singleton == null) {
            singleton = new VolleyConnector(activity, showProgressBar);
        }
        singleton.showProgressBar = showProgressBar;
        return singleton;
    }

    public static VolleyConnector getInstance() {
        return singleton;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public void addToQueue(Request request) {
        mRequestQueue.add(request);
        if(this.showProgressBar){
            progressBar.show();
        }
    }

    public void execute(int method, String url, JSONObject jsonRequest, final IVolleyResponse iVolleyResponse){
        JsonObjectRequest request = new JsonObjectRequest(
                method, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(showProgressBar){
                    progressBar.cancel();
                }
                iVolleyResponse.processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(showProgressBar){
                    progressBar.cancel();
                }
                if(error.toString().equals(TimeoutError.class.getName())){
                    Toast.makeText(activity, "The server not responding please try again.", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(activity, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        addToQueue(request);
    }

    public void destroy(){
        singleton = null;
    }
}

package com.cpn.flickr.refresh;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import com.cpn.flickr.progressbar.ProgressBar;

/**
 * Created by Carlos on 27/01/2017.
 */

public class VolleyConnectorHeader {
    private static VolleyConnectorHeader singleton = null;
    private Context context;
    private RequestQueue mRequestQueue;
    private ProgressBar progressBar;
    private Boolean showProgressBar;

    private VolleyConnectorHeader(Context context, Boolean showProgressBar) {
        mRequestQueue = Volley.newRequestQueue(context);
        this.context = context;
        this.showProgressBar = showProgressBar;
        progressBar = new ProgressBar(context);
    }

    public static VolleyConnectorHeader getInstance(Context context, Boolean showProgressBar) {
        if (singleton == null) {
            singleton = new VolleyConnectorHeader(context, showProgressBar);
        }
        return singleton;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public void addToQueue(Request request) {
        mRequestQueue.add(request);
        if(this.showProgressBar){
            progressBar.show();
        }
    }

    public void execute(int method, String url, JSONObject jsonRequest, final HashMap<String, String> headers, final IVolleyResponse iVolleyResponse){

        JsonObjectRequest request = new JsonObjectRequest(
                method, url, jsonRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(showProgressBar){
                    progressBar.cancel();
                }
                iVolleyResponse.processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(showProgressBar){
                    progressBar.cancel();
                }
                if(error.toString().equals(TimeoutError.class.getName())){
                    Toast.makeText(context, "The server not responding please try again.", Toast.LENGTH_LONG).show();
                }else{
                    //Toast.makeText(activity, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            /*@Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                JSONObject result = new JSONObject();
                try {
                    if(statusCode == 200){
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                        if (jsonString != null && jsonString.length() > 0){
                            result = new JSONObject(jsonString);
                        }

                    }else if(statusCode == 204){
                        result.put("code", statusCode);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
            }*/
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        addToQueue(request);
    }
}
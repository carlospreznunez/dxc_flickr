package com.cpn.flickr.refresh;

import android.content.Context;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;


/**
 * Created by Carlos on 27/01/2017.
 */

public class ConnectivityManager {
    private static ConnectivityManager singleton;
    private AppCompatActivity activity;

    private ConnectivityManager(AppCompatActivity activity){
        this.activity = activity;
    }

    public static ConnectivityManager getInstance(AppCompatActivity activity){
        if(singleton == null){
            singleton = new ConnectivityManager(activity);
        }
        return singleton;
    }

    public Boolean isConnected(){
        android.net.ConnectivityManager cm = (android.net.ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = false;
        if(activeNetwork != null){
            isConnected = activeNetwork.isConnectedOrConnecting();
        }
        return isConnected;
    }

    public void destroy(){
        singleton = null;
    }
}

package com.cpn.flickr.refresh;

import org.json.JSONObject;

/**
 * Created by Carlos on 13/01/2017.
 */

public interface IVolleyResponse {
    void processResponse(JSONObject response);
}
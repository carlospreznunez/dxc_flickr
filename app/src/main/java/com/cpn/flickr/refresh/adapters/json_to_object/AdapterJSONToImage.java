package com.cpn.flickr.refresh.adapters.json_to_object;

import com.cpn.flickr.model.Image;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by Carlos on 12/01/2017.
 */

public class AdapterJSONToImage {
    public static Image convert(JSONObject jsonObject) throws JSONException {
        String title = jsonObject.getString("title");
        String author = jsonObject.getString("ownername");
        String date = jsonObject.getString("dateupload");
        String description = jsonObject.getJSONObject("description").getString("_content");
        String url = "https://farm" + jsonObject.getInt("farm") + ".staticflickr.com/" +
                jsonObject.getString("server") + "/" +
                jsonObject.getString("id") + "_" +
                jsonObject.getString("secret") + ".jpg";

        return new Image(title, author, url, date, description);
    }
}



package com.cpn.flickr.messagebus;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class EventBus {
	private static EventBus singleton;
	private ConcurrentHashMap<MessageEvent, EventSubscriptors> map;

	private EventBus() {
		map = new ConcurrentHashMap<MessageEvent, EventSubscriptors>();
	}
	
	public static EventBus getInstance(){
		if(singleton == null){
			singleton = new EventBus();
		}
		return singleton;
	}
	
	private EventSubscriptors createTypeEvent(MessageEvent event){
        EventSubscriptors subscriptors = new EventSubscriptors();
		map.put(event, subscriptors);
        return subscriptors;
	}
	
	public void addSubscriptor(MessageEvent event, IEventSubscriptor subscriptor){
		EventSubscriptors list = map.get(event);
		if(list == null){
            list = createTypeEvent(event);
		}
        list.addSubscriptor(subscriptor);
	}
	
	public void removeSubscriptor(MessageEvent event, IEventSubscriptor subscriptor){
		EventSubscriptors list = map.get(event);
		if(list != null){
			list.removeSubscriptor(subscriptor);
		}
	}
	
	public void dispatchEvent(MessageEvent event, HashMap<String, Object> args){
		EventSubscriptors listSubscriptors = map.get(event);
		if(listSubscriptors != null){
			listSubscriptors.execute(args);
		}
	}


    public static void destroy() {
        singleton = null;
    }
}

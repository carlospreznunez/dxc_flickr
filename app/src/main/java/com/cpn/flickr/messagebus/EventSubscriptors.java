package com.cpn.flickr.messagebus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventSubscriptors {
	private List<IEventSubscriptor> subscriptors;

	public EventSubscriptors() {
		super();
		this.subscriptors = new ArrayList<IEventSubscriptor>();
	}
	
	public void addSubscriptor(IEventSubscriptor subscriptor){
		this.subscriptors.add(subscriptor);
	}
	
	public void removeSubscriptor(IEventSubscriptor subscriptor){
		this.subscriptors.remove(subscriptor);
	}
	
	public void execute(HashMap<String, Object> args){
		for(IEventSubscriptor subscriptor : this.subscriptors){
			subscriptor.execute(args);
		}
	}
}

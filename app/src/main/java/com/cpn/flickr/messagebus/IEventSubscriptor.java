package com.cpn.flickr.messagebus;

import java.util.HashMap;

public interface IEventSubscriptor {
	
	void execute(HashMap<String, Object> args);

}

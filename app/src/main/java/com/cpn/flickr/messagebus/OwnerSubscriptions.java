package com.cpn.flickr.messagebus;

import java.util.ArrayList;
import java.util.List;

public class OwnerSubscriptions {
    List<Subscription> mySuscriptions;

    public OwnerSubscriptions(){
        mySuscriptions = new ArrayList<Subscription>();
    }

    public void addSubscription(MessageEvent message, IEventSubscriptor subscriptor){
        mySuscriptions.add(new Subscription(message, subscriptor));
        EventBus.getInstance().addSubscriptor(message, subscriptor);
    }

    public void removeSubscriptions(){
        EventBus bus = EventBus.getInstance();
        for(Subscription subscription : mySuscriptions)
            bus.removeSubscriptor(subscription.getEvent(), subscription.getSubscriptor());
    }
    
    public void removeSubscription(Subscription subscription){
    	mySuscriptions.remove(subscription);
    }
}

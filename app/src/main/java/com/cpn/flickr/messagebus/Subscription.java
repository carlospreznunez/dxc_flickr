package com.cpn.flickr.messagebus;

public class Subscription {
    private MessageEvent event;
    private IEventSubscriptor subscriptor;

    public Subscription(MessageEvent event, IEventSubscriptor subscriptor) {
        this.event = event;
        this.subscriptor = subscriptor;
    }

    public MessageEvent getEvent() {
        return event;
    }

    public IEventSubscriptor getSubscriptor() {
        return subscriptor;
    }
}

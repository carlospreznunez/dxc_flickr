package com.cpn.flickr.progressbar;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.cpn.flickr.R;
import com.cpn.flickr.utils.StringManager;

public class ProgressBar {
	private ProgressDialog progress;
	private Context context;

	public ProgressBar(Context context){
		this.context = context;
	}

	public void show(){
		if(!((AppCompatActivity)context).isFinishing()){
			progress = ProgressDialog.show(context, "", StringManager.getInstance().getString(R.string.loading), true);
		}
	}

	public void cancel(){
		progress.dismiss();
	}
}

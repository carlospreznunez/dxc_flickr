package com.cpn.flickr.controller;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import com.cpn.flickr.ActionWindow;
import com.cpn.flickr.controller.subscriptors.ChangeWindowSubscriptor;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.MessageEvent;

public class InitializeApp {
	private static InitializeApp singleton;
	private AppCompatActivity activity;
	HashMap<String, Object> parameters;

	private InitializeApp(AppCompatActivity activity){
		this.activity = activity;
	}

	private void start(){
		parameters = new HashMap<String, Object>();
		showImagesWindow();
	}

	private void showImagesWindow(){
		parameters.put(ChangeWindowSubscriptor.ACTION, ActionWindow.SHOW_LIST_IMAGES);
		EventBus.getInstance().dispatchEvent(MessageEvent.CHANGE_WINDOW_REQUEST, parameters);
	}

	public static void create(AppCompatActivity activity){
		if(singleton == null){
			singleton = new InitializeApp(activity);
			singleton.start();
		}
	}

	public static InitializeApp getInstance(){
		return singleton;
	}

	public void destroy(){
		singleton = null;
	}
}
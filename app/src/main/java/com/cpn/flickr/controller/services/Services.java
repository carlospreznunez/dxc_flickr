package com.cpn.flickr.controller.services;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Services {
	ConcurrentHashMap<ServiceName, IService> map;

	public Services() {
		map = new ConcurrentHashMap<ServiceName, IService>();
	}
	
	public void addService(IService service){
		map.put(service.getNameService(), service);
	}
	
	public boolean execute(ServiceName message, HashMap<String, Object> parameters){
		boolean result = false;
		IService service = map.get(message);
		if(service != null)
			result = service.executeCommand(parameters);
		return result;
	}
	
	
	
}

package com.cpn.flickr.controller;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import com.cpn.flickr.controller.services.*;
import com.cpn.flickr.controller.subscriptors.*;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.refresh.VolleyConnector;
import com.cpn.flickr.utils.StringManager;

public class Controller {
	private static Controller singleton;
	private Services services;
	private AppCompatActivity activity;

	private Controller(AppCompatActivity activity){
		this.activity = activity;
		subscribeBus();
		initializeServices();
	}

	public static Controller create(AppCompatActivity activity){
		if(singleton == null){
			singleton = new Controller(activity);
			StringManager.create(activity);
			singleton.initApp();
		}
		return singleton;
	}

	public void initApp(){
		InitializeApp.create(activity);
	}

	private void subscribeBus() {
		EventBus bus = EventBus.getInstance();
		bus.addSubscriptor(MessageEvent.CHANGE_WINDOW_REQUEST, new ChangeWindowSubscriptor());
		bus.addSubscriptor(MessageEvent.CHANGE_WINDOW_RESPONSE, new ChangeWindowResponseSubscriptor(activity.getSupportFragmentManager()));
	}

	private void initializeServices() {
		services = new Services();
	}

	public void executeService(ServiceName message, HashMap<String, Object> parameters){
		services.execute(message, parameters);
	}

	public static Controller getInstance() {
		return singleton;
	}

	public void destroy() {
		singleton = null;
		services = null;
		StringManager.getInstance().destroy();
		EventBus.destroy();
		if(InitializeApp.getInstance() != null){
			InitializeApp.getInstance().destroy();
		}
		if(VolleyConnector.getInstance() != null){
			VolleyConnector.getInstance().destroy();
		}
	}
}
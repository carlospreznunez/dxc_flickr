package com.cpn.flickr.controller.subscriptors;

public enum StackOption {
	RESET, STACK, REPLACE, REMOVE_ALL_LESS_FIRST; 
}

package com.cpn.flickr.controller.subscriptors;

import androidx.fragment.app.Fragment;

public class FragmentOption {
	private Fragment fragment;
	private StackOption option;
	
	public FragmentOption(Fragment fragment, StackOption option){
		this.fragment = fragment;
		this.option = option;
	}
	
	public Fragment getFragment(){
		return fragment;
	}
	
	public StackOption getOption(){
		return option;
	}
}

package com.cpn.flickr.controller.subscriptors;

import android.transition.TransitionInflater;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.cpn.flickr.R;
import com.cpn.flickr.messagebus.IEventSubscriptor;

public class ChangeWindowResponseSubscriptor implements IEventSubscriptor {

    private FragmentManager fragmentManager;

    public ChangeWindowResponseSubscriptor(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

	@Override
	public void execute(HashMap<String, Object> args) {
		Fragment fragment = (Fragment) args.get("fragment");
		StackOption addStack = (StackOption) args.get("addStack");
		Boolean customTransition = (Boolean) args.get("sharedTransition");

		if(customTransition == null){
			customTransition = false;
		}

		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		if(customTransition){
			defineSharedTransition(args, fragment, fragmentTransaction);
		}else {
			fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
			if (addStack == StackOption.STACK) {
				fragmentTransaction
						.replace(R.id.content_main_activity_fragment, fragment, fragment.getClass().getSimpleName())
						.addToBackStack(null)
						.commit();
			} else if (addStack == StackOption.REPLACE) {
				fragmentManager.popBackStack();
				fragmentTransaction
						.replace(R.id.content_main_activity_fragment, fragment, fragment.getClass().getSimpleName())
						.addToBackStack(null)
						.commit();

			} else if (addStack == StackOption.RESET) {
				for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
					fragmentManager.popBackStack();
				}
				fragmentTransaction
						.replace(R.id.content_main_activity_fragment, fragment, fragment.getClass().getSimpleName())
						.commit();

			} else if (addStack == StackOption.REMOVE_ALL_LESS_FIRST) {
				for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
					fragmentManager.popBackStack();
				}
				fragmentTransaction
						.replace(R.id.content_main_activity_fragment, fragment, fragment.getClass().getSimpleName())
						.addToBackStack(null)
						.commit();
			}
		}
	}

	private void defineSharedTransition(HashMap<String, Object> args, Fragment fragment, FragmentTransaction fragmentTransaction) {
		View viewShared = (View)args.get("sharedView");
		String transitionName = (String)args.get("transitionName");

		List<Fragment> fragments = fragmentManager.getFragments();

		List<Fragment> fragmentsCopy = new ArrayList<>();
		for (Fragment f : fragments) {
			if (f != null){
				fragmentsCopy.add(f);
			}
		}

		int currentFragmentPosition = (fragmentsCopy.size() - 1);
		Fragment currentFragment = fragmentsCopy.get(currentFragmentPosition);

		currentFragment.setSharedElementReturnTransition(TransitionInflater.from(currentFragment.getContext()).inflateTransition(android.R.transition.move));
		currentFragment.setExitTransition(TransitionInflater.from(currentFragment.getContext()).inflateTransition(android.R.transition.fade));

		fragment.setSharedElementEnterTransition(TransitionInflater.from(currentFragment.getContext()).inflateTransition(android.R.transition.move));
		fragment.setEnterTransition(TransitionInflater.from(currentFragment.getContext()).inflateTransition(android.R.transition.explode));

		//fragment.setSharedElementReturnTransition(TransitionInflater.from(fragment.getContext()).inflateTransition(android.R.transition.move));

		fragmentTransaction
				.addSharedElement(viewShared, transitionName)
				.replace(R.id.content_main_activity_fragment, fragment, fragment.getClass().getSimpleName())
				.addToBackStack(null)
				.commit();
	}
}

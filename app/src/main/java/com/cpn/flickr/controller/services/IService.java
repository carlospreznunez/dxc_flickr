package com.cpn.flickr.controller.services;

import java.util.HashMap;

public interface IService {
	
	public ServiceName getNameService();
	
	public boolean executeCommand(HashMap<String, Object> parameters);

}

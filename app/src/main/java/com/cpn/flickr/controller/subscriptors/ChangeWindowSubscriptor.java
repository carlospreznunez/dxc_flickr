package com.cpn.flickr.controller.subscriptors;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import java.util.HashMap;
import com.cpn.flickr.ActionWindow;
import com.cpn.flickr.app.images.ImageDetailFragment;
import com.cpn.flickr.app.images.ImageZoomFragment;
import com.cpn.flickr.app.images.ImagesFragment;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.IEventSubscriptor;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.model.Image;

public class ChangeWindowSubscriptor implements IEventSubscriptor {
	public static final String ACTION = "action";
	public static final String STACK_OPTION = "stackOption";
	private HashMap<ActionWindow, FragmentOption> fragments;

	public ChangeWindowSubscriptor(){
		fragments = new HashMap<>();
		fragments.put(ActionWindow.SHOW_LIST_IMAGES, new FragmentOption(new ImagesFragment(), StackOption.RESET));
		fragments.put(ActionWindow.SHOW_DETAIL_IMAGE, new FragmentOption(new ImageDetailFragment(), StackOption.STACK));
		fragments.put(ActionWindow.SHOW_ZOOM_IMAGE, new FragmentOption(new ImageZoomFragment(), StackOption.STACK));
	}

	@Override
	public void execute(HashMap<String, Object> args) {
		ActionWindow actionWindow = (ActionWindow)args.get(ACTION);
		StackOption stackOption = (StackOption)args.get(STACK_OPTION);
		FragmentOption fragmentOpt = getFragment(args, actionWindow);
		
        args.put("fragment", fragmentOpt.getFragment());
		if(stackOption != null){
			args.put("addStack", stackOption);
		}else{
			args.put("addStack", fragmentOpt.getOption());
		}
        EventBus.getInstance().dispatchEvent(MessageEvent.CHANGE_WINDOW_RESPONSE, args);
    }

	private FragmentOption getFragment(HashMap<String, Object> args, ActionWindow actionWindow) {
		FragmentOption fragmentOpt = fragments.get(actionWindow);
		if(actionWindow == ActionWindow.SHOW_DETAIL_IMAGE || actionWindow == ActionWindow.SHOW_ZOOM_IMAGE){
            Fragment frag = fragmentOpt.getFragment();
            Bundle bundleArgs = new Bundle();
            Image image = (Image)args.get("image");
            bundleArgs.putSerializable("image", image);
            frag.setArguments(bundleArgs);
        }
		return fragmentOpt;
	}
}
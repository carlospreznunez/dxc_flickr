package com.cpn.flickr.controller.subscriptors;

import androidx.fragment.app.FragmentActivity;

import java.util.HashMap;

import com.cpn.flickr.messagebus.IEventSubscriptor;

/**
 * Created by Carlos on 12/04/2017.
 */

public class ResetAppSuscriptor implements IEventSubscriptor {
    private FragmentActivity activity;

    public ResetAppSuscriptor(FragmentActivity activity){
        this.activity = activity;
    }
    @Override
    public void execute(HashMap<String, Object> args) {
        activity.recreate();
    }
}

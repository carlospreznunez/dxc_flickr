package com.cpn.flickr.controller.subscriptors;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.cpn.flickr.R;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.IEventSubscriptor;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.model.Image;
import com.cpn.flickr.refresh.IVolleyResponse;
import com.cpn.flickr.refresh.VolleyConnector;
import com.cpn.flickr.refresh.adapters.json_to_object.AdapterJSONToImage;
import com.cpn.flickr.utils.StringManager;
import com.cpn.flickr.model.Error;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImagesFlickrSuscriptor implements IEventSubscriptor, IVolleyResponse {
    private Integer per_page = 20;
    private AppCompatActivity activity;
    private String term;

    public ImagesFlickrSuscriptor(AppCompatActivity activity){ this.activity = activity; }

    @Override
    public void execute(HashMap<String, Object> args) {
        String url_enpoint = StringManager.getInstance().getString(R.string.url_endpoint);
        String api_key = StringManager.getInstance().getString(R.string.api_key);
        term = (String)args.get("term");
        sanitizeTerm();
        Integer page = 1;
        if(args.containsKey("page")){
            page = (Integer)args.get("page");
        }
        Boolean showProgressBar = true;
        if(args.containsKey("showProgressBar")){
            showProgressBar = (Boolean) args.get("showProgressBar");
        }

        String url = url_enpoint + "?method=flickr.photos.search" +
                "&api_key=" + api_key +
                "&text=" + term +
                "&format=json&sort=relevance&nojsoncallback=1&extras=date_upload,description,url_n,url_l,owner_name&per_page=" + per_page + "&page=" + page;
        VolleyConnector.getInstance(activity, showProgressBar).execute(Request.Method.GET, url, null, this);
    }

    @Override
    public void processResponse(JSONObject response) {
        try{
            String stat = response.getString("stat");
            if(stat.equals("ok")){
                JSONObject jsonObject = response.getJSONObject("photos");
                int page = jsonObject.getInt("page");
                int pages = jsonObject.getInt("pages");
                List<Image> images = new ArrayList<Image>();
                if(page <= pages){
                    JSONArray jsonArray = jsonObject.getJSONArray("photo");
                    images = new ArrayList<Image>();
                    for(int i = 0; i < jsonArray.length(); i++){
                        images.add(AdapterJSONToImage.convert(jsonArray.getJSONObject(i)));
                    }
                }
                HashMap<String, Object> params = new HashMap<>();
                params.put("images", images);
                params.put("term", term);
                EventBus.getInstance().dispatchEvent(MessageEvent.GET_IMAGES_FLICKR_RESPONSE, params);

            }else{
                Error error = new Error(response.getInt("code"), response.getString("message"));
                showErrorDialog(error);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void showErrorDialog(Error error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Error");
        builder.setMessage(error.getMessage());

        String positiveText = "Ok";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sanitizeTerm(){
        term = term.replace(" ", "%20");
    }
}

package com.cpn.flickr.error;

/**
 * Created by Carlos on 09/02/2017.
 */

public enum ApiError {
    API_TOKEN_HAS_EXPIRED, API_TREATMENT_NOT_EXISTS;
}

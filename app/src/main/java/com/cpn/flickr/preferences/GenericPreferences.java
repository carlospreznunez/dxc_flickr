package com.cpn.flickr.preferences;

public interface GenericPreferences<T> {
	String getName();
	void setParameter(String key, T value);
	T getParameter(String key, T defaultValue);
	void removeParameter(String key);
}
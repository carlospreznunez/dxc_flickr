package com.cpn.flickr.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class BooleanPreferences extends Activity implements GenericPreferences<Boolean>{
	public static final String SHOW_INTRO = "showIntro";
	
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	
	public BooleanPreferences(Context context){
		sharedPreferences = context.getSharedPreferences(getName(), MODE_PRIVATE);
	}

	@Override
	public String getName() {
		return "BooleanPreferences";
	}

	@Override
	public void setParameter(String key, Boolean value) {
		editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	@Override
	public Boolean getParameter(String key, Boolean defaultValue) {
		return sharedPreferences.getBoolean(key, defaultValue);
	}

	@Override
	public void removeParameter(String key) {
		if(sharedPreferences.contains(key)){
			editor = sharedPreferences.edit();
			editor.remove(key);
			editor.commit();
		}
	}
}
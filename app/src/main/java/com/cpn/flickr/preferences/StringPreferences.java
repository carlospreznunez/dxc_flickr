package com.cpn.flickr.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Carlos on 11/01/2017.
 */

public class StringPreferences extends Activity implements GenericPreferences<String> {
    public static final String USER = "user";
    public static final String ALARM_HISTORY = "alarm_history";
    public static final String ALARM_CONFIG = "alarm_config";
    public static final String ALARM = "alarm";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public StringPreferences(Context context){
        sharedPreferences = context.getSharedPreferences(getName(), MODE_PRIVATE);
    }

    @Override
    public String getName() {
        return "StringPreferences";
    }

    @Override
    public void setParameter(String key, String value) {
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @Override
    public String getParameter(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    @Override
    public void removeParameter(String key) {
        if(sharedPreferences.contains(key)){
            editor = sharedPreferences.edit();
            editor.remove(key);
            editor.commit();
        }
    }
}
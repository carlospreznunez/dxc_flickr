package com.cpn.flickr.app.images;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cpn.flickr.ActionWindow;
import com.cpn.flickr.R;
import com.cpn.flickr.controller.subscriptors.ChangeWindowSubscriptor;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.model.Image;
import com.cpn.flickr.utils.DateManager;
import com.otaliastudios.zoom.ZoomImageView;

import java.util.HashMap;

public class ImageDetailFragment extends Fragment {
    private AppCompatActivity activity;
    private Image image;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity)getActivity();
        image = getImage();
    }

    private Image getImage() {
        Bundle arguments = getArguments();
        Image image;
        if(arguments != null){
            image = ((Image) arguments.getSerializable("image"));
        }else{
            image = new Image("Undefined", "Undefined", "", "", "Undefined");
        }
        return image;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_detail, container, false);
        initializeActionBar();
        initializeComponents(view);
        return view;
    }

    private void initializeComponents(View view) {
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        TextView textViewAuthor = (TextView) view.findViewById(R.id.textViewAuthor);
        TextView textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        TextView textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);

        if(image.getImageBitmap() != null){
            imageView.setImageBitmap(image.getImageBitmap());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String,Object> params = new HashMap<>();
                    params.put(ChangeWindowSubscriptor.ACTION, ActionWindow.SHOW_ZOOM_IMAGE);
                    params.put("image", image);
                    EventBus.getInstance().dispatchEvent(MessageEvent.CHANGE_WINDOW_REQUEST, params);
                }
            });
        }

        textViewTitle.setText(image.getTitle());
        textViewAuthor.setText(image.getAuthor());
        textViewDate.setText(DateManager.getFormattedDateForServerFromTimeStamp(Long.parseLong(image.getDate())));
        textViewDescription.setText(image.getDescription());
    }

    private void initializeActionBar(){
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(R.string.images_detail_title);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

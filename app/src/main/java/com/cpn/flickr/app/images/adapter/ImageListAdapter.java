package com.cpn.flickr.app.images.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cpn.flickr.ActionWindow;
import com.cpn.flickr.R;
import com.cpn.flickr.controller.subscriptors.ChangeWindowSubscriptor;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.model.Image;

import java.util.HashMap;
import java.util.List;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> implements ItemClickListener  {
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Image> images;
    private View currentSharedView;

    public ImageListAdapter(List<Image> images){
        this.images = images;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_image_item, parent, false);
            return new ImageViewHolder(view, this);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new ImageViewHolder(view, this);
        }
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, final int position) {
        if(getItemViewType(position) == ITEM){
            String title = images.get(position).getTitle();
            String author = images.get(position).getAuthor();
            String image_url = images.get(position).getUrl();
            Bitmap imageBitmap = images.get(position).getImageBitmap();

            holder.textViewTitle.setText(title);
            holder.textViewAuthor.setText(author);

            if( (!image_url.equals("")) && (imageBitmap == null) ){
                Glide.with(holder.view)
                        .asBitmap()
                        .load(image_url)
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                holder.imageView.setImageBitmap(resource);
                                images.get(position).setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
            }else if(imageBitmap != null){
                holder.imageView.setImageBitmap(imageBitmap);
            }
            holder.imageView.setTransitionName(position + "_imageTransition");
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(images.get(position) != null){
            return ITEM;
        }else{
            return LOADING;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        currentSharedView = view.findViewById(R.id.imageItem);
        Image image = images.get(position);
        HashMap<String,Object> params = new HashMap<>();
        params.put(ChangeWindowSubscriptor.ACTION, ActionWindow.SHOW_DETAIL_IMAGE);
        params.put("image", image);

        params.put("sharedTransition", true);
        params.put("sharedView", currentSharedView);
        params.put("transitionName", "imageTransition");

        EventBus.getInstance().dispatchEvent(MessageEvent.CHANGE_WINDOW_REQUEST, params);
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        View view;
        TextView textViewTitle;
        TextView textViewAuthor;
        ImageView imageView;
        ItemClickListener listener;

        ImageViewHolder(View v, ItemClickListener listener) {
            super(v);
            view = v;
            textViewTitle = v.findViewById(R.id.textViewTitle);
            textViewAuthor = v.findViewById(R.id.textViewAuthor);
            imageView = v.findViewById(R.id.imageItem);
            v.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(getAdapterPosition(), view);
        }
    }

    public void setImages(List<Image> images){
        this.images = images;
    }
}

interface ItemClickListener {
    void onItemClick(int position, View view);
}
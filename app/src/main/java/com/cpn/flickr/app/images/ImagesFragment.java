package com.cpn.flickr.app.images;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.cpn.flickr.R;
import com.cpn.flickr.app.images.adapter.ImageListAdapter;
import com.cpn.flickr.controller.subscriptors.ImagesFlickrSuscriptor;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.IEventSubscriptor;
import com.cpn.flickr.messagebus.MessageEvent;
import com.cpn.flickr.messagebus.OwnerSubscriptions;
import com.cpn.flickr.model.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImagesFragment extends Fragment implements IEventSubscriptor {
    private AppCompatActivity activity;
    private OwnerSubscriptions subscriptions;
    private RecyclerView recyclerView;
    private ImageListAdapter adapter;
    private View empty;
    private List<Image> images;
    private View view;
    private boolean isLoading;
    private String term;
    private Integer currentPage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        images = new ArrayList<Image>();
        isLoading = false;
        currentPage = 1;
        activity = (AppCompatActivity)getActivity();
        setHasOptionsMenu(true);
        initializeSuscriptors();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_images, container, false);
        initializeActionBar();
        initializeComponents(view);
        initScrollListener();
        return view;
    }

    private void initializeSuscriptors() {
        subscriptions = new OwnerSubscriptions();
        subscriptions.addSubscription(MessageEvent.GET_IMAGES_FLICKR_REQUEST, new ImagesFlickrSuscriptor(activity));
        subscriptions.addSubscription(MessageEvent.GET_IMAGES_FLICKR_RESPONSE, this);
    }

    private void initializeComponents(View view) {
        recyclerView = (RecyclerView)view.findViewById(R.id.reciclerViewHistory);
        empty = view.findViewById(R.id.empty);

        if(images.size() == 0){
            recyclerView.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        }
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new ImageListAdapter(images);
        recyclerView.setAdapter(adapter);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == images.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        images.add(null);
        adapter.notifyItemInserted(images.size() - 1);
        currentPage = currentPage + 1;
        HashMap<String,Object> params = new HashMap<>();
        params.put("term", term);
        params.put("page", currentPage);
        params.put("showProgressBar", false);
        EventBus.getInstance().dispatchEvent(MessageEvent.GET_IMAGES_FLICKR_REQUEST, params);
    }

    @Override
    public void execute(HashMap<String, Object> args) {
        List<Image> aux_images = (List<Image>)args.get("images");
        term = (String)args.get("term");
        if(isLoading){
            images.remove(images.size() - 1);
            int scrollPosition = images.size();
            adapter.notifyItemRemoved(scrollPosition);
            if(aux_images.size() > 0){
                this.images.addAll(aux_images);
                adapter.notifyDataSetChanged();
                isLoading = false;
            }
        }else{
            images = aux_images;
            recyclerView.setVisibility(View.VISIBLE);
            empty.setVisibility(View.GONE);
            adapter.setImages(images);
            adapter.notifyDataSetChanged();
            currentPage = 1;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);
    }

    private void initializeActionBar(){
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        activity.getSupportActionBar().setTitle(R.string.images_actionBar_title);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        subscriptions.removeSubscriptions();
    }
}

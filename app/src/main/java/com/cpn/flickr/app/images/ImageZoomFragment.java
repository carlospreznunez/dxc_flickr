package com.cpn.flickr.app.images;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cpn.flickr.R;
import com.cpn.flickr.model.Image;
import com.otaliastudios.zoom.ZoomImageView;

public class ImageZoomFragment extends Fragment {
    private Image image;
    private AppCompatActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity)getActivity();
        image = getImage();
    }

    private Image getImage() {
        Bundle arguments = getArguments();
        Image image;
        if(arguments != null){
            image = ((Image) arguments.getSerializable("image"));
        }else{
            image = new Image("Undefined", "Undefined", "", "", "Undefined");
        }
        return image;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_zoom, container, false);
        initializeActionBar();
        initializeComponents(view);
        return view;
    }

    private void initializeActionBar() {
        activity.getSupportActionBar().setTitle(R.string.images_zoom_title);
    }

    private void initializeComponents(View view) {
        ZoomImageView zoomImageView = (ZoomImageView) view.findViewById(R.id.zoomImageView);
        zoomImageView.setImageBitmap(image.getImageBitmap());
    }
}

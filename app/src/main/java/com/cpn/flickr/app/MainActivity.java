package com.cpn.flickr.app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;
import com.cpn.flickr.R;
import com.cpn.flickr.controller.Controller;
import com.cpn.flickr.messagebus.EventBus;
import com.cpn.flickr.messagebus.MessageEvent;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Controller.create(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent){
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            getImagesByString(query);
        }
    }

    private void getImagesByString(String term){
        HashMap<String,Object> params = new HashMap<>();
        params.put("term", term);
        EventBus.getInstance().dispatchEvent(MessageEvent.GET_IMAGES_FLICKR_REQUEST, params);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Controller.getInstance().destroy();
    }
}
